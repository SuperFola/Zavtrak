# Zavtrak

Zavtrak means "Breakfast" in Russian, and yes, I am not a Russian.

Zavtrak is rendering engine based on OpenGL, aiming at a (nearly) zero cost abstraction.

# What can it do for me ?

* creating a Window (GLFW)

* handling events (keyboard, mouse, joystick)

* time management

* loading textures and shaders

# Examples

Some examples can be found under the folder `examples/`. To choose which one should be compiled, modify the `CMakeLists.txt`.

## Output

* hello_triangle.cpp

[![hello_triangle.cpp output](images/hello_triangle.png)](https://gitlab.com/SuperFola/Zavtrak/tree/master/examples/hello_triangle.cpp)

* texture.cpp

[![hello_texture.cpp output](images/hello_texture.png)](https://gitlab.com/SuperFola/Zavtrak/tree/master/examples/hello_texture.cpp)

* hello_3d.cpp (it's rotating)

[![hello_3d.cpp output](images/hello_3d.png)](https://gitlab.com/SuperFola/Zavtrak/tree/master/examples/hello_3d.cpp)

* hello_cubes.cpp (it's rotating too)

[![hello_cubes.cpp output](images/hello_cubes.png)](https://gitlab.com/SuperFola/Zavtrak/tree/master/examples/hello_cubes.cpp)

* hello_light.cpp (the light cube is rotating)

[![hello_light.cpp output](images/hello_light.png)](https://gitlab.com/SuperFola/Zavtrak/tree/master/examples/hello_light.cpp)

# Building

You need :

* GL

* GLU

* GLFW3

* pthread

* Xi

* Xrandr

* cmake, make

```bash
cd PROJECT_DIRECTORY
mkdir -p build/ && cd build
cmake .. && make
```

# Default ressources provided

Shaders to apply color, texture with or without a model-view-projection.

Images in resources/ can be found on http://learnopengl.com

# Credits

* learnopengl.com for the images and their great tutorial

* Sean Barrett for `stb_image.h`