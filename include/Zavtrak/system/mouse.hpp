#ifndef zk_system_mouse
#define zk_system_mouse

#include <GLFW/glfw3.h>

#include <tuple>
#include <vector>
#include <functional>

#include <Zavtrak/common/events.hpp>

namespace zk
{
    namespace system
    {
        class Mouse
        {
        public:
            using MouseMoveCallback = std::function<void(double, double)>;
            using MouseClickCallback = std::vector<
                std::tuple<
                    zk::Button,
                    zk::EventType,
                    std::function<void(double, double)>
                >
            >;
            using Callbacks_t = std::tuple<
                MouseMoveCallback,
                MouseClickCallback
                >;
        
        private:
            GLFWwindow* m_window{};
            Callbacks_t m_callbacks;
        
        public:
            Mouse();
            Mouse(GLFWwindow* window);

            void setCursorPos(double x, double y);
            void getCursorPos(double* x, double* y);
            void setShowCursor(zk::CursorState cursor_state);

            void setMouseMoveCallback(std::function<void(double, double)> callback);
            void addMouseClicCallback(zk::Button button, zk::EventType type, std::function<void(double, double)> callback);

            const Callbacks_t& getUserData();
        };
    }
}

#endif