#ifndef zk_system_clock
#define zk_system_clock

namespace zk
{
    namespace system
    {
        class Clock
        {
        private:
            double m_time{};
            double m_last{};

        public:
            Clock();
            ~Clock();

            void tick();
            double timeSinceLastTick() const;
            double time() const;
        };
    }
}

#endif