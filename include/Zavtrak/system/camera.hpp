#ifndef zk_system_camera
#define zk_system_camera

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace zk
{
    namespace system
    {
        enum class CameraMovement
        {
            Forward,
            Backward,
            Right,
            Left
        };

        class Camera
        {
        private:
            glm::vec3 m_pos, m_worldup, m_front, m_right, m_up;
            float m_yaw, m_pitch, m_fovy, m_speed, m_sensivity;
            glm::mat4 m_view{};
            float m_locked_at;
        
        public:
            Camera(glm::vec3 position=glm::vec3(0.0f, 0.0f, 0.0f), float yaw=-90.0f, float pitch=0.0f, float fovy=45.0f);
            ~Camera();

            void enableFreeFly();
            void disableFreeFly(float locked_at_y=0.0f);

            float getFovy();
            void setFovy(float val);

            float getSpeed();
            void setSpeed(float val);

            float getSensivity();
            void setSensivity(float val);

            void updateVectors();

            void processKeyboard(CameraMovement direction, float dt);
            void processMouseMovement(float x, float y);

            void move(const glm::vec3& offset);
            void move(glm::vec3&& offset);
            void rotateY(float angle);

            void setY(float val);

            const glm::mat4& getViewMatrix();
            const glm::vec3& getPos();
            const glm::vec3& getFrontVec();
        };
    }
}

#endif