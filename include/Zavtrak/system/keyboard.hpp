#ifndef zk_system_keyboard
#define zk_system_keyboard

#include <GLFW/glfw3.h>

#include <tuple>
#include <vector>
#include <functional>

#include <Zavtrak/common/events.hpp>

namespace zk
{
    namespace system
    {
        class Keyboard
        {
        public:
            using Callbacks_t = std::vector<
                std::tuple<
                    int, zk::Key, std::function<void()>
                >>;
        
        private:
            GLFWwindow* m_window{};
            Callbacks_t m_callbacks;

            void updateCallbacks();
        
        public:
            Keyboard();
            Keyboard(GLFWwindow* window);

            void addCallback(int eventtype, zk::Key key, std::function<void()> callback);
        
            const Callbacks_t& getUserData();
        };
    }
}

#endif