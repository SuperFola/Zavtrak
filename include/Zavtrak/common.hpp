#ifndef zk_common
#define zk_common

#include <Zavtrak/common/colors.hpp>
#include <Zavtrak/common/common.hpp>
#include <Zavtrak/common/cubemap.hpp>
#include <Zavtrak/common/events.hpp>
#include <Zavtrak/common/indices.hpp>
#include <Zavtrak/common/shader.hpp>
#include <Zavtrak/common/texture.hpp>
#include <Zavtrak/common/vertices.hpp>

#endif