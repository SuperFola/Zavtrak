#ifndef zk_system
#define zk_system

#include <Zavtrak/system/camera.hpp>
#include <Zavtrak/system/clock.hpp>
#include <Zavtrak/system/keyboard.hpp>
#include <Zavtrak/system/mouse.hpp>
#include <Zavtrak/system/window.hpp>

#endif