#ifndef zk_common_cubemap
#define zk_common_cubemap

#include <GLFW/glfw3.h>
#include <string>

namespace zk
{
    namespace common
    {
        class Cubemap
        {
        private:
            unsigned int m_texture;

        public:
            Cubemap();
            Cubemap(const std::string& top, const std::string& bottom, 
                    const std::string& front, const std::string& back,
                    const std::string& right, const std::string& left);
            ~Cubemap();

            void loadFromFiles(const std::string& top, const std::string& bottom, 
                               const std::string& front, const std::string& back,
                               const std::string& right, const std::string& left);
            
            void bind(int index=-1);
            void unbind();
        };
    }
}

#endif