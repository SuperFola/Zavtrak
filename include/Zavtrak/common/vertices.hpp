#ifndef zk_common_vertices
#define zk_common_vertices

#include <vector>

namespace zk
{
    class Vertices
    {
    private:
        std::vector<float> m_points{};

    public:
        Vertices();
        Vertices(const std::vector<float>& points);

        void loadFromVector(const std::vector<float>& points);

        const std::vector<float>& get();
        float* getData();

        std::size_t count() const;
    };
}

#endif