#ifndef zk_common_shader
#define zk_common_shader

#include <GLFW/glfw3.h>
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace zk
{
    class Shader
    {
    private:
        unsigned int m_program{};

    public:
        Shader();
        Shader(const std::string& vertex_file_path, const std::string& fragment_file_path);
        ~Shader();

        void loadFromFile(const std::string& vertex_file_path, const std::string& fragment_file_path);
        void loadFromString(const std::string& vertex_shader, const std::string& fragment_shader);
        const unsigned int getProgram();
        void use();

        template <typename T> void setUniform1(const std::string& name, T a);
        template <typename T> void setUniform2(const std::string& name, T a, T b);
        template <typename T> void setUniform3(const std::string& name, T a, T b, T c);
        template <typename T> void setUniform4(const std::string& name, T a, T b, T c, T d);
        void setMat2(const std::string& name, const glm::mat2& mat);
        void setMat3(const std::string& name, const glm::mat3& mat);
        void setMat4(const std::string& name, const glm::mat4& mat);
    };
}

#endif