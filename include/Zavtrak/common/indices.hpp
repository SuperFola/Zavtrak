#ifndef zk_common_indices
#define zk_common_indices

#include <vector>

namespace zk
{
    class Indices
    {
    private:
        std::vector<unsigned int> m_indices{};
    
    public:
        Indices();
        Indices(const std::vector<unsigned int>& indices);
        ~Indices();

        const std::vector<unsigned int> get();
        unsigned int* getData();

        std::size_t count() const;
    };
}

#endif