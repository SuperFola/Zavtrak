#ifndef zk_common_texture
#define zk_common_texture

#include <GLFW/glfw3.h>
#include <stb/stb_image.h>
#include <string>

#include <Zavtrak/common/colors.hpp>

namespace zk
{
    class Texture
    {
    private:
        int m_width{}, m_height{}, m_channels{};
        unsigned int m_texture{};

    public:
        Texture();
        Texture(const std::string& filename);
        Texture(const std::string& filename, zk::Color fallback_color);
        Texture(const std::string& filename, bool alpha);
        Texture(zk::Color color);
        ~Texture();

        void loadFromFile(const std::string& filename, zk::Color fallback_color=zk::Color::White, bool alpha=false, int maxMipMap=7);
        void createPlainColorTexture(zk::Color color);

        void bind(int id=-1);
        void unbind();

        int getWidth() const;
        int getHeight() const;
    };
}

#endif