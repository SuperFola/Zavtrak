#ifndef zk_objects
#define zk_objects

#include <Zavtrak/objects/common.hpp>
#include <Zavtrak/objects/element_buffer.hpp>
#include <Zavtrak/objects/vertex_array.hpp>
#include <Zavtrak/objects/vertex_buffer.hpp>

#endif