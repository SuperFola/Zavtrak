#ifndef zk_objects_common
#define zk_objects_common

#include <GLFW/glfw3.h>

namespace zk
{
    enum class DrawType
    {
        Stream,
        Static,
        Dynamic
    };
}

#endif