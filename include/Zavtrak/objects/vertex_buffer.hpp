#ifndef zk_objects_vertex_buffer
#define zk_objects_vertex_buffer

#include <GLFW/glfw3.h>

#include <vector>

#include <Zavtrak/common/common.hpp>
#include <Zavtrak/objects/common.hpp>
#include <Zavtrak/common/vertices.hpp>

namespace zk
{
    namespace objects
    {
        class VertexBuffer
        {
        private:
            unsigned int m_index{};
        
        public:
            VertexBuffer();
            ~VertexBuffer();

            void bind();
            void unbind();

            void setData(zk::Vertices& data, DrawType draw_type=DrawType::Static);

            unsigned int getIndex() const;
        };

        void setVertexAttrib(int index, int component_count, int total_size, int offset);
    }
}

#endif