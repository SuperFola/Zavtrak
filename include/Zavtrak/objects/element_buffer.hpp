#ifndef zk_objects_element_buffer
#define zk_objects_element_buffer

#include <GLFW/glfw3.h>

#include <vector>

#include <Zavtrak/common/common.hpp>
#include <Zavtrak/objects/common.hpp>
#include <Zavtrak/common/indices.hpp>

namespace zk
{
    namespace objects
    {
        class ElementBuffer
        {
        private:
            unsigned int m_index{};

        public:
            ElementBuffer();
            ~ElementBuffer();

            void bind();
            void unbind();

            unsigned int getIndex() const;

            void setData(zk::Indices& indices, DrawType draw_type=DrawType::Static);
        };
    }
}

#endif