#ifndef zk_objects_vertex_array
#define zk_objects_vertex_array

#include <GLFW/glfw3.h>

namespace zk
{
    namespace objects
    {
        class VertexArray
        {
        private:
            unsigned int m_index{};
        
        public:
            VertexArray();
            ~VertexArray();

            void bind();
            void unbind();

            unsigned int getIndex() const;
        };
    }
}

#endif