#include <Zavtrak/system.hpp>
#include <Zavtrak/common.hpp>
#include <Zavtrak/objects.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>
#include <string>

template <typename T> void echo(T str)
{
    std::cout << str << std::endl;
}

constexpr unsigned int WIDTH = 800, HEIGHT = 600;

int main(int argc, char** argv)
{
    auto window = zk::system::Window(WIDTH, HEIGHT, "Zavtrak", zk::system::Window::RESIZE);
    auto& keyboard_ref = window.getKeyboard();
    auto& mouse_ref = window.getMouse();
    auto clock = zk::system::Clock();
    auto texture = zk::Texture("resources/wall.jpg");

    auto lightingShader = zk::Shader("shaders/lights/diffuse/shader.vert", "shaders/lights/diffuse/shader.frag");
    auto lampShader = zk::Shader();
    lampShader.loadFromString(
        // vertex shader
        "#version 330 core\n"
        "layout (location = 0) in vec3 aPos;\n"
        "uniform mat4 model;\n"
        "uniform mat4 view;\n"
        "uniform mat4 projection;\n"
        "void main() {\n"
        "	gl_Position = projection * view * model * vec4(aPos, 1.0);\n"
        "}\n",

        // fragment shader
        "#version 330 core\n"
        "out vec4 FragColor;\n"
        "void main() {\n"
        "    FragColor = vec4(1.0); // set all 4 vector values to 1.0\n"
        "}\n"
    );

    auto cubemap = zk::common::Cubemap("resources/skybox/top.jpg", "resources/skybox/bottom.jpg",
                                       "resources/skybox/front.jpg", "resources/skybox/back.jpg",
                                       "resources/skybox/right.jpg", "resources/skybox/left.jpg");
    auto skyboxShader = zk::Shader();
    skyboxShader.loadFromString(
        // vertex shader
        "#version 330 core\n"
        "layout (location = 0) in vec3 aPos;\n"
        "out vec3 TexCoords;\n"
        "uniform mat4 projection;\n"
        "uniform mat4 view;\n"
        "void main() {\n"
        "    TexCoords = aPos;\n"
        "    gl_Position = projection * view * vec4(aPos, 1.0);\n"
        "}\n",

        // fragment shader
        "#version 330 core\n"
        "out vec4 FragColor;\n"
        "in vec3 TexCoords;\n"
        "uniform samplerCube skybox;\n"
        "void main() {\n"
        "    FragColor = texture(skybox, TexCoords);\n"
        "}\n"
    );

    glm::vec3 lightPos = glm::vec3(0.0f, 4.0f, 0.0f);

    auto camera = zk::system::Camera();
    camera.setSpeed(50.0f);
    camera.rotateY(-45.0f);
    camera.move(glm::vec3(1.0f, 1.0f, 3.0f));
    camera.updateVectors();

    keyboard_ref.addCallback(zk::EventType::Pressed, zk::Key::Escape, [&window] () { window.close(); });
    keyboard_ref.addCallback(zk::EventType::Pressed, zk::Key::G, [] () { echo("G: pressed"); });
    keyboard_ref.addCallback(zk::EventType::Released, zk::Key::G, [] () { echo("G: released"); });
    keyboard_ref.addCallback(zk::EventType::Pressed, zk::Key::T, [&mouse_ref] () { mouse_ref.setShowCursor(zk::CursorState::Visible); });

    keyboard_ref.addCallback(zk::EventType::Repeat, zk::Key::W, [&camera, &clock, &lightingShader, &lampShader] () {
        camera.processKeyboard(zk::system::CameraMovement::Forward, clock.timeSinceLastTick());
        camera.updateVectors();
        lightingShader.use(); lightingShader.setMat4("view", camera.getViewMatrix());
        lampShader.use(); lampShader.setMat4("view", camera.getViewMatrix());
    });
    keyboard_ref.addCallback(zk::EventType::Repeat, zk::Key::S, [&camera, &clock, &lightingShader, &lampShader] () {
        camera.processKeyboard(zk::system::CameraMovement::Backward, clock.timeSinceLastTick());
        camera.updateVectors();
        lightingShader.use(); lightingShader.setMat4("view", camera.getViewMatrix());
        lampShader.use(); lampShader.setMat4("view", camera.getViewMatrix());
    });
    keyboard_ref.addCallback(zk::EventType::Repeat, zk::Key::A, [&camera, &clock, &lightingShader, &lampShader] () {
        camera.processKeyboard(zk::system::CameraMovement::Left, clock.timeSinceLastTick());
        camera.updateVectors();
        lightingShader.use(); lightingShader.setMat4("view", camera.getViewMatrix());
        lampShader.use(); lampShader.setMat4("view", camera.getViewMatrix());
    });
    keyboard_ref.addCallback(zk::EventType::Repeat, zk::Key::D, [&camera, &clock, &lightingShader, &lampShader] () {
        camera.processKeyboard(zk::system::CameraMovement::Right, clock.timeSinceLastTick());
        camera.updateVectors();
        lightingShader.use(); lightingShader.setMat4("view", camera.getViewMatrix());
        lampShader.use(); lampShader.setMat4("view", camera.getViewMatrix());
    });

    mouse_ref.setShowCursor(zk::CursorState::Disabled);
    double ox{WIDTH / 2.0}, oy{HEIGHT / 2.0};
    mouse_ref.setMouseMoveCallback([&camera, &lightingShader, &lampShader, &ox, &oy] (double x, double y) {
        camera.processMouseMovement(x - ox, oy - y);
        camera.updateVectors();
        lightingShader.use(); lightingShader.setMat4("view", camera.getViewMatrix());
        lampShader.use(); lampShader.setMat4("view", camera.getViewMatrix());
        ox = x; oy = y;
    });

    window.updateCallbacks();

    zk::Vertices vertices({
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,

        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f, 0.0f,

        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,

        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f
    });

    // configuring cube VAO and VBO
    auto cubeVAO = zk::objects::VertexArray();
    auto VBO = zk::objects::VertexBuffer();

    // bind & set
    VBO.setData(vertices, zk::DrawType::Static);

    cubeVAO.bind();

    zk::objects::setVertexAttrib(0, 3, 8, 0);  // position
    zk::objects::setVertexAttrib(1, 3, 8, 3);  // normal
    zk::objects::setVertexAttrib(2, 2, 8, 6);  // texture

    // configuring light's VAO (VBO says the same, vertices are the same for the light object which is also a 3D cube)
    auto lightVAO = zk::objects::VertexArray();
    lightVAO.bind();

    VBO.bind();
    zk::objects::setVertexAttrib(0, 3, 8, 0);

    // configure skybox
    zk::Vertices skyboxVertices({
        // positions          
        -1.0f,  1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,

        -1.0f, -1.0f,  1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f,  1.0f,
        -1.0f, -1.0f,  1.0f,

        1.0f, -1.0f, -1.0f,
        1.0f, -1.0f,  1.0f,
        1.0f,  1.0f,  1.0f,
        1.0f,  1.0f,  1.0f,
        1.0f,  1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,

        -1.0f, -1.0f,  1.0f,
        -1.0f,  1.0f,  1.0f,
        1.0f,  1.0f,  1.0f,
        1.0f,  1.0f,  1.0f,
        1.0f, -1.0f,  1.0f,
        -1.0f, -1.0f,  1.0f,

        -1.0f,  1.0f, -1.0f,
        1.0f,  1.0f, -1.0f,
        1.0f,  1.0f,  1.0f,
        1.0f,  1.0f,  1.0f,
        -1.0f,  1.0f,  1.0f,
        -1.0f,  1.0f, -1.0f,

        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
        1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
        1.0f, -1.0f,  1.0f
    });
    auto skyboxVAO = zk::objects::VertexArray();
    auto skyboxVBO = zk::objects::VertexBuffer();
    skyboxVAO.bind();
    skyboxVBO.setData(skyboxVertices);
    zk::objects::setVertexAttrib(0, 3, 3, 0);
    skyboxShader.use();
    skyboxShader.setUniform1<int>("skybox", 0);

    camera.updateVectors();
    
    lightingShader.use();
    lightingShader.setUniform1<int>("texture1", 0);
    lightingShader.setUniform3<float>("objectColor", 1.0f, 0.5f, 0.31f);
    lightingShader.setUniform3<float>("lightColor", 1.0f, 1.0f, 1.0f);
    // view / projection transformations
    glm::mat4 projection = glm::perspective(glm::radians(camera.getFovy()), window.getAspectRatio(), 0.1f, 100.0f);
    lightingShader.setMat4("projection", projection);
    lightingShader.setMat4("view", camera.getViewMatrix());

    window.onResize([&projection, &camera, &window] (int w, int h) {
        projection = glm::perspective(glm::radians(camera.getFovy()), window.getAspectRatio(), 0.1f, 100.0f);
    });
    window.updateCallbacks();

    // also draw the lamp object
    lampShader.use();
    lampShader.setMat4("projection", projection);
    lampShader.setMat4("view", camera.getViewMatrix());

    while(!window.shouldClose())
    {
        // events handling
        window.pollEvents();

        // delta time handling
        clock.tick();
        double dt = clock.timeSinceLastTick();
        //std::cout << "dt: " << dt << ", fps: " << 1 / dt << "\r";
        
        // rendering
        // first, clear screen
        window.clear(zk::Color(20, 20, 30));

        // render skybox
        glDepthMask(GL_FALSE);
        skyboxShader.use();
        skyboxShader.setMat4("view", glm::mat4(glm::mat3(camera.getViewMatrix())));  // removing translations
        skyboxShader.setMat4("projection", projection);
        skyboxVAO.bind();
        cubemap.bind(0);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glDepthMask(GL_TRUE);

        // change the light's position
        lightPos.x = std::cos(clock.time() / 4.0) * 4.0;
        lightPos.z = std::sin(clock.time() / 4.0) * 4.0;

        // activate shader, set uniforms
        texture.bind(0);
        lightingShader.use();
        lightingShader.setUniform3<float>("lightPos", lightPos.x, lightPos.y, lightPos.z);

        // render the cube
        cubeVAO.bind();
        for (int i=-5; i < 5; ++i)
        {
            for (int j=-5; j < 5; ++j)
            {
                // create transformations
                glm::mat4 model = glm::mat4(1.0f);
                model = glm::translate(model, glm::vec3((float) i, 0.0f, (float) j));
                // pass it to the shader
                lightingShader.setMat4("model", model);
                glDrawArrays(GL_TRIANGLES, 0, 36);
            }
        }

        lampShader.use();
        glm::mat4 model(1.0f);
        model = glm::translate(model, lightPos);
        model = glm::scale(model, glm::vec3(0.2f));  // a smaller cube
        lampShader.setMat4("model", model);

        lightVAO.bind();
        glDrawArrays(GL_TRIANGLES, 0, 36);

        // swap buffers
        window.display();
    }

    return 0;
}