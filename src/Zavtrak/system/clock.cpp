#include <GLFW/glfw3.h>

#include <Zavtrak/system/clock.hpp>

namespace zk
{
    namespace system
    {
        Clock::Clock() :
            m_time(0.0), m_last(0.0)
        {}

        Clock::~Clock()
        {}

        void Clock::tick()
        {
            m_last = m_time;
            m_time = glfwGetTime();
        }

        double Clock::timeSinceLastTick() const
        {
            return m_time - m_last;
        }

        double Clock::time() const
        {
            return m_time;
        }
    }
}