#include <Zavtrak/system/camera.hpp>
#include <iostream>

namespace zk
{
    namespace system
    {
        Camera::Camera(glm::vec3 position, float yaw, float pitch, float fovy) :
            m_pos(position), m_worldup(glm::vec3(0.0f, 1.0f, 0.0f)), m_front(0.0f, 0.0f, -1.0f),
            m_yaw(yaw), m_pitch(pitch), m_fovy(fovy), m_speed(2.5f), m_sensivity(0.1f), m_locked_at(-1.0f)
        {
            updateVectors();
        }

        Camera::~Camera()
        {}

        void Camera::enableFreeFly()
        {
            m_locked_at = -1.0f;
        }

        void Camera::disableFreeFly(float locked_at_y)
        {
            if (locked_at_y < 0.0f)
                std::cerr << "(zk::system::Camera::disableFreeFly) locked_at_y must be >= 0.0f" << std::endl;
            else
                m_locked_at = locked_at_y;
        }

        float Camera::getFovy()
        {
            return m_fovy;
        }

        void Camera::setFovy(float val)
        {
            m_fovy = val;
        }

        float Camera::getSpeed()
        {
            return m_speed;
        }

        void Camera::setSpeed(float val)
        {
            if (val > 0.1f)
                m_speed = val;
        }

        float Camera::getSensivity()
        {
            return m_sensivity;
        }

        void Camera::setSensivity(float val)
        {
            if (m_sensivity > 0.01f)
                m_sensivity = val;
        }

        void Camera::updateVectors()
        {
            m_front.x = std::cos(glm::radians(m_yaw)) * std::cos(glm::radians(m_pitch));
            m_front.y = std::sin(glm::radians(m_pitch));
            m_front.z = std::sin(glm::radians(m_yaw)) * std::cos(glm::radians(m_pitch));
            m_front = glm::normalize(m_front);

            m_right = glm::normalize(glm::cross(m_front, m_worldup));
            m_up = glm::normalize(glm::cross(m_right, m_front));

            m_view = glm::lookAt(m_pos, m_pos + m_front, m_up);
        }

        void Camera::processKeyboard(CameraMovement direction, float dt)
        {
            float velocity = m_speed * dt;

            if (direction == CameraMovement::Forward)
                m_pos += m_front * velocity;
            else if (direction == CameraMovement::Backward)
                m_pos -= m_front * velocity;
            else if (direction == CameraMovement::Left)
                m_pos -= m_right * velocity;
            else if (direction == CameraMovement::Right)
                m_pos += m_right * velocity;
            
            if (m_locked_at >= 0.0f)
                m_pos.y = m_locked_at;  // make sure the user stays at ground level
        }

        void Camera::processMouseMovement(float x, float y)
        {
            x *= m_sensivity;
            y *= m_sensivity;

            m_yaw = glm::mod(m_yaw + x, 360.0f);
            m_pitch += y;

            if (m_pitch > 89.0f) m_pitch = 89.0f;
            if (m_pitch < -89.0f) m_pitch = -89.0f;
        }

        void Camera::move(const glm::vec3& offset)
        {
            m_pos += offset;
        }

        void Camera::move(glm::vec3&& offset)
        {
            m_pos += offset;
        }

        void Camera::rotateY(float angle)
        {
            m_yaw = glm::mod(m_yaw + angle, 360.0f);
        }

        void Camera::setY(float val)
        {
            m_pos.y = val;
        }

        const glm::mat4& Camera::getViewMatrix()
        {
            return m_view;
        }

        const glm::vec3& Camera::getPos()
        {
            return m_pos;
        }

        const glm::vec3& Camera::getFrontVec()
        {
            return m_front;
        }
    }
}