#include <glad/glad.h>

#include <Zavtrak/common/texture.hpp>
#include <exception>
#include <stdexcept>
#include <iostream>

namespace zk
{
    Texture::Texture() :
        m_width(0), m_height(0), m_channels(0), m_texture(0)
    {}

    Texture::~Texture()
    {
        if (m_texture != 0)
        {
            glBindTexture(GL_TEXTURE_2D, 0);
            glDeleteTextures(1, &m_texture);
        }
    }

    Texture::Texture(const std::string& filename)
    {
        loadFromFile(filename);
    }

    Texture::Texture(const std::string& filename, zk::Color fallback_color)
    {
        loadFromFile(filename, fallback_color);
    }

    Texture::Texture(const std::string& filename, bool alpha)
    {
        loadFromFile(filename, zk::Color::White, alpha);
    }

    Texture::Texture(zk::Color color)
    {
        createPlainColorTexture(color);
    }

    void Texture::loadFromFile(const std::string& filename, zk::Color fallback_color, bool alpha, int maxMipMap)
    {
        glGenTextures(1, &m_texture);

        unsigned char* data = stbi_load(filename.c_str(), &m_width, &m_height, &m_channels, alpha ? 4 : 0);
        if (data != nullptr)
        {
            bind();
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, maxMipMap);
            glTexImage2D(
                GL_TEXTURE_2D,
                // mipmap level
                0,
                // image only has RGB values (or RGBA)
                alpha ? GL_RGBA : GL_RGB,
                // width & height of the corresponding texture
                m_width, m_height,
                // always 0, some legacy stuff
                0,
                // we loaded the image with RGB(A) values into unsigned chars
                alpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE,
                // the actual image data
                data
            );
            glGenerateMipmap(GL_TEXTURE_2D);
            unbind();
        }
        else
        {
            std::cerr << "(zk::common::Texture::loadFromFile) Could not load texture '" << filename << "'" << std::endl;
            bind();
            // make a coloured square
            uint8_t* color = new uint8_t[3];
            if (color == nullptr)
                throw std::runtime_error("(zk::common::Texture::loadFromFile) Could not create color");
            color[0] = fallback_color.r; color[1] = fallback_color.g; color[2] = fallback_color.b;
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1, 1, 0, GL_RGB, GL_UNSIGNED_BYTE, color);
            unbind();
            delete[] color;
        }
        stbi_image_free(data);
    }

    void Texture::createPlainColorTexture(zk::Color color)
    {
        glGenTextures(1, &m_texture);

        bind();
        // make a coloured square
        uint8_t* c = new uint8_t[3];
        if (c == nullptr)
            throw std::runtime_error("(zk::common::Texture::loadFromFile) Could not create color");
        c[0] = color.r; c[1] = color.g; c[2] = color.b;
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1, 1, 0, GL_RGB, GL_UNSIGNED_BYTE, c);
        unbind();
        delete[] c;
    }

    void Texture::bind(int id)
    {
        if (id != -1)
            glActiveTexture(GL_TEXTURE0 + id);
        glBindTexture(GL_TEXTURE_2D, m_texture);
    }

    void Texture::unbind()
    {
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    int Texture::getWidth() const
    {
        return m_width;
    }

    int Texture::getHeight() const
    {
        return m_height;
    }
}