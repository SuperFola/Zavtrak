#include <glad/glad.h>

#include <Zavtrak/common/shader.hpp>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <exception>
#include <stdexcept>

namespace zk
{
    Shader::Shader() :
        m_program(0)
    {}

    Shader::Shader(const std::string& vertex_file_path, const std::string& fragment_file_path)
    {
        loadFromFile(vertex_file_path, fragment_file_path);
    }

    Shader::~Shader()
    {}

    void Shader::loadFromFile(const std::string& vertex_file_path, const std::string& fragment_file_path)
    {
        // Read the Vertex Shader code from the file
        std::string VertexShaderCode = "";
        std::ifstream VertexShaderStream(vertex_file_path);
        if (VertexShaderStream.is_open())
        {
            std::stringstream sstr;
            sstr << VertexShaderStream.rdbuf();
            VertexShaderCode = sstr.str();
            VertexShaderStream.close();
        }
        else
            throw std::runtime_error("(zk::Shader::load) Can not find file: " + vertex_file_path);

        // Read the Fragment Shader code from the file
        std::string FragmentShaderCode = "";
        std::ifstream FragmentShaderStream(fragment_file_path);
        if (FragmentShaderStream.is_open())
        {
            std::stringstream sstr;
            sstr << FragmentShaderStream.rdbuf();
            FragmentShaderCode = sstr.str();
            FragmentShaderStream.close();
        }
        else
            throw std::runtime_error("(zk::Shader::load) Can not find file: " + fragment_file_path);
        
        loadFromString(VertexShaderCode, FragmentShaderCode);
    }

    void Shader::loadFromString(const std::string& vertex_shader, const std::string& fragment_shader)
    {
        // Create the shaders
        unsigned int VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
        unsigned int FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

        GLint Result = GL_FALSE;
        int InfoLogLength = 0;

        // Compile Vertex Shader
        const char* VertexSourcePointer = vertex_shader.c_str();
        glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
        glCompileShader(VertexShaderID);

        // Check Vertex Shader
        glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
        glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
        if (InfoLogLength > 0 && Result == GL_FALSE)
        {
            std::vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
            glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
            std::cerr << "(zk::Shader::load) " << &VertexShaderErrorMessage[0] << std::endl;
            throw std::runtime_error("Vertex shader has errors");
        }

        // Compile Fragment Shader
        const char* FragmentSourcePointer = fragment_shader.c_str();
        glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
        glCompileShader(FragmentShaderID);

        // Check Fragment Shader
        Result = GL_FALSE;
        glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
        glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
        if (InfoLogLength > 0 && Result == GL_FALSE)
        {
            std::vector<char> FragmentShaderErrorMessage(InfoLogLength + 1);
            glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
            if (FragmentShaderErrorMessage[0] != 0)
            {
                std::cerr << "(zk::Shader::load) " << &FragmentShaderErrorMessage[0] << std::endl;
                throw std::runtime_error("Fragment shader has errors");
            }
        }

        // Link the program
        m_program = glCreateProgram();
        glAttachShader(m_program, VertexShaderID);
        glAttachShader(m_program, FragmentShaderID);
        glLinkProgram(m_program);

        // Check the program
        Result = GL_FALSE;
        glGetProgramiv(m_program, GL_LINK_STATUS, &Result);
        glGetProgramiv(m_program, GL_INFO_LOG_LENGTH, &InfoLogLength);
        if (InfoLogLength > 0 && Result == GL_FALSE)
        {
            std::vector<char> ProgramErrorMessage(InfoLogLength+1);
            glGetProgramInfoLog(m_program, InfoLogLength, NULL, &ProgramErrorMessage[0]);
            if (ProgramErrorMessage[0] != 0)
            {
                std::cerr << "(zk::Shader::load) " << &ProgramErrorMessage[0] << std::endl;
                throw std::runtime_error("Program shader has errors");
            }
        }
        
        glDetachShader(m_program, VertexShaderID);
        glDetachShader(m_program, FragmentShaderID);
        
        glDeleteShader(VertexShaderID);
        glDeleteShader(FragmentShaderID);
    }

    const unsigned int Shader::getProgram()
    {
        return m_program;
    }

    void Shader::use()
    {
        glUseProgram(m_program);
    }

    // specifications for setUniform1[f|d|i|ui]
    template <> void Shader::setUniform1<float>(const std::string& name, float a)
    {
        int uniform = glGetUniformLocation(m_program, name.c_str());
        glUniform1f(uniform, a);
    }

    template <> void Shader::setUniform1<double>(const std::string& name, double a)
    {
        int uniform = glGetUniformLocation(m_program, name.c_str());
        glUniform1d(uniform, a);
    }

    template <> void Shader::setUniform1<int>(const std::string& name, int a)
    {
        int uniform = glGetUniformLocation(m_program, name.c_str());
        glUniform1i(uniform, a);
    }

    template <> void Shader::setUniform1<unsigned int>(const std::string& name, unsigned int a)
    {
        int uniform = glGetUniformLocation(m_program, name.c_str());
        glUniform1ui(uniform, a);
    }

    // specifications for setUniform2[f|d|i|ui]
    template <> void Shader::setUniform2<float>(const std::string& name, float a, float b)
    {
        int uniform = glGetUniformLocation(m_program, name.c_str());
        glUniform2f(uniform, a, b);
    }

    template <> void Shader::setUniform2<double>(const std::string& name, double a, double b)
    {
        int uniform = glGetUniformLocation(m_program, name.c_str());
        glUniform2d(uniform, a, b);
    }

    template <> void Shader::setUniform2<int>(const std::string& name, int a, int b)
    {
        int uniform = glGetUniformLocation(m_program, name.c_str());
        glUniform2i(uniform, a, b);
    }

    template <> void Shader::setUniform2<unsigned int>(const std::string& name, unsigned int a, unsigned int b)
    {
        int uniform = glGetUniformLocation(m_program, name.c_str());
        glUniform2ui(uniform, a, b);
    }

    // specifications for setUniform3[f|d|i|ui]
    template <> void Shader::setUniform3<float>(const std::string& name, float a, float b, float c)
    {
        int uniform = glGetUniformLocation(m_program, name.c_str());
        glUniform3f(uniform, a, b, c);
    }

    template <> void Shader::setUniform3<double>(const std::string& name, double a, double b, double c)
    {
        int uniform = glGetUniformLocation(m_program, name.c_str());
        glUniform3d(uniform, a, b, c);
    }

    template <> void Shader::setUniform3<int>(const std::string& name, int a, int b, int c)
    {
        int uniform = glGetUniformLocation(m_program, name.c_str());
        glUniform3i(uniform, a, b, c);
    }

    template <> void Shader::setUniform3<unsigned int>(const std::string& name, unsigned int a, unsigned int b, unsigned int c)
    {
        int uniform = glGetUniformLocation(m_program, name.c_str());
        glUniform3ui(uniform, a, b, c);
    }

    // specifications for setUniform4[f|d|i|ui]
    template <> void Shader::setUniform4<float>(const std::string& name, float a, float b, float c, float d)
    {
        int uniform = glGetUniformLocation(m_program, name.c_str());
        glUniform4f(uniform, a, b, c, d);
    }

    template <> void Shader::setUniform4<double>(const std::string& name, double a, double b, double c, double d)
    {
        int uniform = glGetUniformLocation(m_program, name.c_str());
        glUniform4d(uniform, a, b, c, d);
    }

    template <> void Shader::setUniform4<int>(const std::string& name, int a, int b, int c, int d)
    {
        int uniform = glGetUniformLocation(m_program, name.c_str());
        glUniform4i(uniform, a, b, c, d);
    }

    template <> void Shader::setUniform4<unsigned int>(const std::string& name, unsigned int a, unsigned int b, unsigned int c, unsigned int d)
    {
        int uniform = glGetUniformLocation(m_program, name.c_str());
        glUniform4ui(uniform, a, b, c, d);
    }

    void Shader::setMat2(const std::string& name, const glm::mat2& mat)
    {
        glUniformMatrix2fv(glGetUniformLocation(m_program, name.c_str()), 1, GL_FALSE, glm::value_ptr(mat));
    }

    void Shader::setMat3(const std::string& name, const glm::mat3& mat)
    {
        glUniformMatrix3fv(glGetUniformLocation(m_program, name.c_str()), 1, GL_FALSE, glm::value_ptr(mat));
    }

    void Shader::setMat4(const std::string& name, const glm::mat4& mat)
    {
        glUniformMatrix4fv(glGetUniformLocation(m_program, name.c_str()), 1, GL_FALSE, glm::value_ptr(mat));
    }
}