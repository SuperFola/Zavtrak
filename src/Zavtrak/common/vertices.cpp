#include <Zavtrak/common/vertices.hpp>

namespace zk
{
    Vertices::Vertices()
    {}

    Vertices::Vertices(const std::vector<float>& points) :
        m_points(points)
    {}

    void Vertices::loadFromVector(const std::vector<float>& points)
    {
        m_points = points;
    }

    const std::vector<float>& Vertices::get()
    {
        return m_points;
    }

    float* Vertices::getData()
    {
        return &m_points[0];
    }

    std::size_t Vertices::count() const
    {
        return m_points.size();
    }
}