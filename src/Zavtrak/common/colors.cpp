#include <Zavtrak/common/colors.hpp>

const zk::Color zk::Color::Black(0, 0, 0);
const zk::Color zk::Color::White(255, 255, 255);
const zk::Color zk::Color::Red(255, 0, 0);
const zk::Color zk::Color::Green(0, 255, 0);
const zk::Color zk::Color::Blue(0, 0, 255);
const zk::Color zk::Color::Yellow(255, 255, 0);
const zk::Color zk::Color::Magenta(255, 0, 255);
const zk::Color zk::Color::Cyan(0, 255, 255);
const zk::Color zk::Color::Transparent(0, 0, 0, 0);