#include <glad/glad.h>

#include <Zavtrak/common/cubemap.hpp>
#include <stb/stb_image.h>

#include <vector>
#include <exception>
#include <stdexcept>
#include <iostream>

namespace zk
{
    namespace common
    {
        Cubemap::Cubemap() :
            m_texture(0)
        {}

        Cubemap::Cubemap(const std::string& top, const std::string& bottom, 
                    const std::string& front, const std::string& back,
                    const std::string& right, const std::string& left)
        {
            loadFromFiles(top, bottom, front, back, right, left);
        }

        Cubemap::~Cubemap()
        {
            if (m_texture != 0)
            {
                glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
                glDeleteTextures(1, &m_texture);
            }
        }

        void Cubemap::loadFromFiles(const std::string& top, const std::string& bottom, 
                                    const std::string& front, const std::string& back,
                                    const std::string& right, const std::string& left)
        {
            glGenTextures(1, &m_texture);
            bind();

            uint8_t* color = new uint8_t[3];
            if (color == nullptr)
                throw std::runtime_error("(zk::common::Cubemap::loadFromFiles) Could not create color");
            
            std::vector<std::string> files = {
                right, left,
                top, bottom,
                front, back
            };

            int width{}, height{}, channels{};
            for (std::size_t i=0; i < files.size(); ++i)
            {
                unsigned char* data = stbi_load(files[i].c_str(), &width, &height, &channels, 0);
                if (data != nullptr)
                {
                    glTexImage2D(
                        GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
                        0,
                        GL_RGB,
                        width, height,
                        0,
                        GL_RGB,
                        GL_UNSIGNED_BYTE,
                        data
                    );
                }
                else
                {
                    std::cerr << "(zk::common::Cubemap::loadFromFiles) Could not load cubemap texture '" << files[i] << "'" << std::endl;
                    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, 1, 1, 0, GL_RGB, GL_UNSIGNED_BYTE, color);
                }
                stbi_image_free(data);
            }

            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

            delete[] color;
        }

        void Cubemap::bind(int index)
        {
            if (index != -1)
                glActiveTexture(GL_TEXTURE0 + index);
            glBindTexture(GL_TEXTURE_CUBE_MAP, m_texture);
        }

        void Cubemap::unbind()
        {
            glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
        }
    }
}