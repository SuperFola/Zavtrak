#include <Zavtrak/common/indices.hpp>

namespace zk
{
    Indices::Indices()
    {}

    Indices::Indices(const std::vector<unsigned int>& indices) :
        m_indices(indices)
    {}

    Indices::~Indices()
    {}

    const std::vector<unsigned int> Indices::get()
    {
        return m_indices;
    }

    unsigned int* Indices::getData()
    {
        return &m_indices[0];
    }

    std::size_t Indices::count() const
    {
        return m_indices.size();
    }
}