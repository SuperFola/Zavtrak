#include <glad/glad.h>

#include <Zavtrak/objects/element_buffer.hpp>

namespace zk
{
    namespace objects
    {
        ElementBuffer::ElementBuffer()
        {
            glGenBuffers(1, &m_index);
        }

        ElementBuffer::~ElementBuffer()
        {
            glDeleteBuffers(1, &m_index);
        }

        void ElementBuffer::bind()
        {
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_index);
        }

        void ElementBuffer::unbind()
        {
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        }

        unsigned int ElementBuffer::getIndex() const
        {
            return m_index;
        }

        void ElementBuffer::setData(zk::Indices& indices, DrawType draw_type)
        {
            bind();
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.count() * sizeof(unsigned int), indices.getData(), (int) draw_type);
        }
    }
}