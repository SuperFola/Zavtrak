#include <glad/glad.h>

#include <Zavtrak/objects/vertex_buffer.hpp>

namespace zk
{
    namespace objects
    {
        VertexBuffer::VertexBuffer()
        {
            glGenBuffers(1, &m_index);
        }

        VertexBuffer::~VertexBuffer()
        {
            glDeleteBuffers(1, &m_index);
        }

        void VertexBuffer::bind()
        {
            glBindBuffer(GL_ARRAY_BUFFER, m_index);
        }

        void VertexBuffer::unbind()
        {
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        }

        void VertexBuffer::setData(zk::Vertices& vertices, DrawType draw_type)
        {
            bind();
            int dt = (draw_type == DrawType::Static) ? GL_STATIC_DRAW : ((draw_type == DrawType::Dynamic) ? GL_DYNAMIC_DRAW : GL_STREAM_DRAW);
            glBufferData(GL_ARRAY_BUFFER, vertices.count() * sizeof(float), vertices.getData(), dt);
        }

        unsigned int VertexBuffer::getIndex() const
        {
            return m_index;
        }

        void setVertexAttrib(int index, int component_count, int total_size, int offset)
        {
            glVertexAttribPointer(
                index,
                component_count,
                GL_FLOAT,  // TODO template the function to be able to choose the type
                GL_FALSE,  // TODO add parameter to set "normalized" ?
                total_size * sizeof(float),  // stride
                (void*)(offset * sizeof(float))   // pointer
            );
            glEnableVertexAttribArray(index);
        }
    }
}