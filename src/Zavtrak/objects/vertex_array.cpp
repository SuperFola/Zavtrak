#include <glad/glad.h>

#include <Zavtrak/objects/vertex_array.hpp>

namespace zk
{
    namespace objects
    {
        VertexArray::VertexArray()
        {
            glGenVertexArrays(1, &m_index);
        }

        VertexArray::~VertexArray()
        {
            glDeleteVertexArrays(1, &m_index);
        }

        void VertexArray::bind()
        {
            glBindVertexArray(m_index);
        }

        void VertexArray::unbind()
        {
            glBindVertexArray(0);
        }

        unsigned int VertexArray::getIndex() const
        {
            return m_index;
        }
    }
}