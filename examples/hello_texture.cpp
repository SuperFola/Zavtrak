#include <Zavtrak/system.hpp>
#include <Zavtrak/common.hpp>
#include <Zavtrak/objects.hpp>

#include <iostream>

int main(int argc, char** argv)
{
    auto window = zk::system::Window(800, 600, "Zavtrak", zk::system::Window::RESIZE);
    auto& keyboard_ref = window.getKeyboard();
    auto clock = zk::system::Clock();
    auto shader = zk::Shader("shaders/texture/shader.vert", "shaders/texture/shader.frag");
    auto texture = zk::Texture("resources/wall.jpg");

    keyboard_ref.addCallback(zk::EventType::Pressed, zk::Key::Escape, [&window] () { window.close(); });
    window.updateCallbacks();

    zk::Vertices vertices({
        // positions          // colors           // texture coords
         0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f, // top right
         0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f, // bottom right
        -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f, // bottom left
        -0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f  // top left 
    });
    zk::Indices indices({  
        0, 1, 3, // first triangle
        1, 2, 3  // second triangle
    });
    zk::objects::VertexArray VAO;
    zk::objects::VertexBuffer VBO;
    zk::objects::ElementBuffer EBO;

    VAO.bind();
    VBO.setData(vertices);

    EBO.bind();
    EBO.setData(indices);

    zk::objects::setVertexAttrib(0, 3, 8, 0);
    zk::objects::setVertexAttrib(1, 3, 8, 3);
    zk::objects::setVertexAttrib(2, 2, 8, 6);

    while(!window.shouldClose())
    {
        // delta time handling
        clock.tick();
        double dt = clock.timeSinceLastTick();
        std::cout << "dt: " << dt << " - FPS: " << 1 / dt << "\r";

        // events handling
        window.pollEvents();
        
        // rendering
        // first, clear screen
        window.clear(zk::Color(50, 70, 70));

        // then render the wall
        texture.bind();
        shader.use();
        VAO.bind();
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

        // swap buffers
        window.display();
    }

    return 0;
}