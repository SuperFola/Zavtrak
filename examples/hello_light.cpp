#include <Zavtrak/system.hpp>
#include <Zavtrak/common.hpp>
#include <Zavtrak/objects.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>
#include <string>

void puts(const std::string& str)
{
    std::cout << str << std::endl;
}

constexpr unsigned int WIDTH = 800, HEIGHT = 600;

int main(int argc, char** argv)
{
    auto window = zk::system::Window(WIDTH, HEIGHT, "Zavtrak", zk::system::Window::RESIZE);
    auto& keyboard_ref = window.getKeyboard();
    auto& mouse_ref = window.getMouse();
    auto clock = zk::system::Clock();

    auto lightingShader = zk::Shader("shaders/lights/diffuse/shader.vert", "shaders/lights/diffuse/shader.frag");
    auto lampShader = zk::Shader();
    lampShader.loadFromString(
        // vertex shader
        "#version 330 core\n"
        "layout (location = 0) in vec3 aPos;\n"
        "uniform mat4 model;\n"
        "uniform mat4 view;\n"
        "uniform mat4 projection;\n"
        "void main() {\n"
        "	gl_Position = projection * view * model * vec4(aPos, 1.0);\n"
        "}\n",

        // fragment shader
        "#version 330 core\n"
        "out vec4 FragColor;\n"
        "void main() {\n"
        "    FragColor = vec4(1.0); // set all 4 vector values to 1.0\n"
        "}\n"
    );

    glm::vec3 lightPos = glm::vec3(1.0f, 0.0f, 0.0f);

    auto camera = zk::system::Camera();
    camera.setSpeed(50.0f);
    camera.rotateY(-45.0f);
    camera.move(glm::vec3(1.0f, 0.0f, 3.0f));
    camera.updateVectors();

    keyboard_ref.addCallback(zk::EventType::Pressed, zk::Key::Escape, [&window] () { window.close(); });
    keyboard_ref.addCallback(zk::EventType::Pressed, zk::Key::G, [] () { puts("G: pressed"); });
    keyboard_ref.addCallback(zk::EventType::Released, zk::Key::G, [] () { puts("G: released"); });

    keyboard_ref.addCallback(zk::EventType::Repeat, zk::Key::Up, [&camera, &clock, &lightingShader, &lampShader] () {
        camera.processKeyboard(zk::system::CameraMovement::Forward, clock.timeSinceLastTick());
        camera.updateVectors();
        lightingShader.use(); lightingShader.setMat4("view", camera.getViewMatrix());
        lampShader.use(); lampShader.setMat4("view", camera.getViewMatrix());
    });
    keyboard_ref.addCallback(zk::EventType::Repeat, zk::Key::Down, [&camera, &clock, &lightingShader, &lampShader] () {
        camera.processKeyboard(zk::system::CameraMovement::Backward, clock.timeSinceLastTick());
        camera.updateVectors();
        lightingShader.use(); lightingShader.setMat4("view", camera.getViewMatrix());
        lampShader.use(); lampShader.setMat4("view", camera.getViewMatrix());
    });
    keyboard_ref.addCallback(zk::EventType::Repeat, zk::Key::Left, [&camera, &clock, &lightingShader, &lampShader] () {
        camera.processKeyboard(zk::system::CameraMovement::Left, clock.timeSinceLastTick());
        camera.updateVectors();
        lightingShader.use(); lightingShader.setMat4("view", camera.getViewMatrix());
        lampShader.use(); lampShader.setMat4("view", camera.getViewMatrix());
    });
    keyboard_ref.addCallback(zk::EventType::Repeat, zk::Key::Right, [&camera, &clock, &lightingShader, &lampShader] () {
        camera.processKeyboard(zk::system::CameraMovement::Right, clock.timeSinceLastTick());
        camera.updateVectors();
        lightingShader.use(); lightingShader.setMat4("view", camera.getViewMatrix());
        lampShader.use(); lampShader.setMat4("view", camera.getViewMatrix());
    });

    mouse_ref.setShowCursor(zk::CursorState::Disabled);
    double ox{WIDTH / 2.0}, oy{HEIGHT / 2.0};
    mouse_ref.setMouseMoveCallback([&camera, &lightingShader, &lampShader, &ox, &oy] (double x, double y) {
        camera.processMouseMovement(x - ox, oy - y);
        camera.updateVectors();
        lightingShader.use(); lightingShader.setMat4("view", camera.getViewMatrix());
        lampShader.use(); lampShader.setMat4("view", camera.getViewMatrix());
        ox = x; oy = y;
    });

    window.updateCallbacks();

    zk::Vertices vertices({
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
         0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,

        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
         0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,

        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,

         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
         0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,

        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f
    });

    // configuring cube VAO and VBO
    auto cubeVAO = zk::objects::VertexArray();
    auto VBO = zk::objects::VertexBuffer();

    // bind & set
    VBO.setData(vertices, zk::DrawType::Static);

    cubeVAO.bind();

    zk::objects::setVertexAttrib(0, 3, 6, 0);  // position
    zk::objects::setVertexAttrib(1, 3, 6, 3);  // normal

    // configuring light's VAO (VBO says the same, vertices are the same for the light object which is also a 3D cube)
    auto lightVAO = zk::objects::VertexArray();
    lightVAO.bind();

    VBO.bind();
    zk::objects::setVertexAttrib(0, 3, 6, 0);

    camera.updateVectors();
    
    lightingShader.use();
    lightingShader.setUniform3<float>("objectColor", 1.0f, 0.5f, 0.31f);
    lightingShader.setUniform3<float>("lightColor", 1.0f, 1.0f, 1.0f);
    // view / projection transformations
    glm::mat4 projection = glm::perspective(glm::radians(camera.getFovy()), window.getAspectRatio(), 0.1f, 100.0f);
    lightingShader.setMat4("projection", projection);
    lightingShader.setMat4("view", camera.getViewMatrix());
    // world transformation
    glm::mat4 model(1.0f);
    lightingShader.setMat4("model", model);

    // also draw the lamp object
    lampShader.use();
    lampShader.setMat4("projection", projection);
    lampShader.setMat4("view", camera.getViewMatrix());

    while(!window.shouldClose())
    {
        // events handling
        window.pollEvents();

        // delta time handling
        clock.tick();
        double dt = clock.timeSinceLastTick();
        
        // rendering
        // first, clear screen
        window.clear(zk::Color(20, 20, 30));

        // change the light's position
        lightPos.x = std::cos(clock.time());
        lightPos.z = std::sin(clock.time());

        // activate shader, set uniforms
        lightingShader.use();
        lightingShader.setUniform3<float>("lightPos", lightPos.x, lightPos.y, lightPos.z);

        // render the cube
        cubeVAO.bind();
        glDrawArrays(GL_TRIANGLES, 0, 36);

        lampShader.use();
        model = glm::mat4(1.0f);
        model = glm::translate(model, lightPos);
        model = glm::scale(model, glm::vec3(0.2f));  // a smaller cube
        lampShader.setMat4("model", model);

        lightVAO.bind();
        glDrawArrays(GL_TRIANGLES, 0, 36);

        // swap buffers
        window.display();
    }

    return 0;
}