#include <Zavtrak/system.hpp>
#include <Zavtrak/common.hpp>
#include <Zavtrak/objects.hpp>

#include <iostream>

int main(int argc, char** argv)
{
    auto window = zk::system::Window(800, 600, "Zavtrak", zk::system::Window::RESIZE);
    auto& keyboard_ref = window.getKeyboard();
    auto clock = zk::system::Clock();
    auto shader = zk::Shader("shaders/color/shader.vert", "shaders/color/shader.frag");

    keyboard_ref.addCallback(zk::EventType::Pressed, zk::Key::Escape, [&window] () { window.close(); });
    window.updateCallbacks();

    zk::Vertices vertices({
        // positions         // colors
         0.5f, -0.5f, 0.0f,  1.0f, 0.0f, 0.0f,   // bottom right
        -0.5f, -0.5f, 0.0f,  0.0f, 1.0f, 0.0f,   // bottom left
         0.0f,  0.5f, 0.0f,  0.0f, 0.0f, 1.0f    // top 
    });
    zk::objects::VertexArray VAO;
    zk::objects::VertexBuffer VBO;
    // bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
    VAO.bind();
    VBO.setData(vertices);
    zk::objects::setVertexAttrib(0, 3, 6, 0);
    zk::objects::setVertexAttrib(1, 3, 6, 3);

    shader.use();

    while(!window.shouldClose())
    {
        // delta time handling
        clock.tick();
        double dt = clock.timeSinceLastTick();
        std::cout << "dt: " << dt << " - FPS: " << 1 / dt << "\r";

        // events handling
        window.pollEvents();
        
        // rendering
        // first, clear screen
        window.clear(zk::Color(50, 70, 70));

        // then render the triangle
        VAO.bind();
        glDrawArrays(GL_TRIANGLES, 0, 3);

        // swap buffers
        window.display();
    }

    return 0;
}