#include <Zavtrak/system.hpp>
#include <Zavtrak/common.hpp>
#include <Zavtrak/objects.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>

int main(int argc, char** argv)
{
    auto window = zk::system::Window(800, 600, "Zavtrak", zk::system::Window::RESIZE);
    auto& keyboard_ref = window.getKeyboard();
    auto clock = zk::system::Clock();
    auto shader = zk::Shader("shaders/3d/shader.vert", "shaders/3d/shader.frag");
    auto texture = zk::Texture("resources/container.jpg");

    keyboard_ref.addCallback(zk::EventType::Pressed, zk::Key::Escape, [&window] () { window.close(); });
    window.updateCallbacks();

    glm::vec3 cubePositions[] = {
        glm::vec3( 0.0f,  0.0f,  0.0f), 
        glm::vec3( 2.0f,  5.0f, -15.0f), 
        glm::vec3(-1.5f, -2.2f, -2.5f),  
        glm::vec3(-3.8f, -2.0f, -12.3f),  
        glm::vec3( 2.4f, -0.4f, -3.5f),  
        glm::vec3(-1.7f,  3.0f, -7.5f),  
        glm::vec3( 1.3f, -2.0f, -2.5f),  
        glm::vec3( 1.5f,  2.0f, -2.5f), 
        glm::vec3( 1.5f,  0.2f, -1.5f), 
        glm::vec3(-1.3f,  1.0f, -1.5f)  
    };
    zk::Vertices vertices({
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
    });
    
    zk::objects::VertexArray VAO;
    zk::objects::VertexBuffer VBO;
    VAO.bind();
    VBO.setData(vertices);
    zk::objects::setVertexAttrib(0, 3, 5, 0);
    zk::objects::setVertexAttrib(1, 2, 5, 3);

    shader.use();
    shader.setUniform1<int>("texture1", 0);

    glm::mat4 view = glm::lookAt(
        glm::vec3(0, 0, 10),  // camera position
        glm::vec3(0, 0, 0),  // and looks at the origin
        glm::vec3(0, 1, 0)   // head is up (set to 0,-1,0 to look upside-down)
    );
    glm::mat4 projection = glm::perspective(glm::radians(45.0f), window.getAspectRatio(), 0.1f, 100.0f);
    shader.setMat4("projection", projection);
    shader.setMat4("view", view);

    float angle = 0.0f;

    while(!window.shouldClose())
    {
        // delta time handling
        clock.tick();
        double dt = clock.timeSinceLastTick();
        std::cout << "dt: " << dt << " - FPS: " << 1 / dt << "\r";

        angle += 35.0f * dt;  // rotation by 35°/second

        // events handling
        window.pollEvents();
        
        // rendering
        // first, clear screen
        window.clear(zk::Color::Cyan);

        // then render the container
        texture.bind(0);
        shader.use();

        // render container
        VAO.bind();
        for (unsigned int i=0; i < 10; ++i)
        {
            // create transformations
            glm::mat4 model = glm::mat4(1.0f);
            model = glm::translate(model, cubePositions[i]);
            model = glm::rotate(model, glm::radians(angle), glm::vec3(1.0f, 0.0f, 1.0f));
            // pass it to the shader
            shader.setMat4("model", model);
            glDrawArrays(GL_TRIANGLES, 0, 36);
        }

        // swap buffers
        window.display();
    }

    return 0;
}