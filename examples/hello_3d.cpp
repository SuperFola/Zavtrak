#include <Zavtrak/system.hpp>
#include <Zavtrak/common.hpp>
#include <Zavtrak/objects.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>

int main(int argc, char** argv)
{
    auto window = zk::system::Window(800, 600, "Zavtrak", zk::system::Window::RESIZE);
    auto& keyboard_ref = window.getKeyboard();
    auto clock = zk::system::Clock();
    auto shader = zk::Shader("shaders/3d/shader.vert", "shaders/3d/shader.frag");
    auto texture = zk::Texture("resources/container.jpg");

    keyboard_ref.addCallback(zk::EventType::Pressed, zk::Key::Escape, [&window] () { window.close(); });
    window.updateCallbacks();

    zk::Vertices vertices({
        // positions          // texture coords
         0.5f,  0.5f, 0.0f,   1.0f, 1.0f, // top right
         0.5f, -0.5f, 0.0f,   1.0f, 0.0f, // bottom right
        -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, // bottom left
        -0.5f,  0.5f, 0.0f,   0.0f, 1.0f  // top left 
    });
    zk::Indices indices({
        0, 1, 3, // first triangle
        1, 2, 3  // second triangle
    });
    zk::objects::VertexArray VAO;
    zk::objects::VertexBuffer VBO;
    zk::objects::ElementBuffer EBO;

    VAO.bind();
    VBO.setData(vertices);
    EBO.setData(indices);
    zk::objects::setVertexAttrib(0, 3, 5, 0);
    zk::objects::setVertexAttrib(1, 2, 5, 3);

    shader.use();
    shader.setUniform1<int>("texture1", 0);

    glm::mat4 view = glm::lookAt(
        glm::vec3(0, 0, 3),  // camera position
        glm::vec3(0, 0, 0),  // and looks at the origin
        glm::vec3(0, 1, 0)   // head is up (set to 0,-1,0 to look upside-down)
    );
    glm::mat4 projection = glm::perspective(glm::radians(45.0f), window.getAspectRatio(), 0.1f, 100.0f);
    shader.setMat4("projection", projection);
    shader.setMat4("view", view);

    float angle = 0.0f;

    while(!window.shouldClose())
    {
        // delta time handling
        clock.tick();
        double dt = clock.timeSinceLastTick();
        std::cout << "dt: " << dt << " - FPS: " << 1 / dt << "\r";

        angle += 10.0f * dt;  // rotation by 10°/second

        // events handling
        window.pollEvents();
        
        // rendering
        // first, clear screen
        window.clear(zk::Color::Cyan);

        // then render the container
        texture.bind(0);
        shader.use();
        // create transformations
        glm::mat4 model = glm::mat4(1.0f);
        model = glm::rotate(model, glm::radians(angle), glm::vec3(1.0f, 0.0f, 1.0f));
        // pass them to the shader
        shader.setMat4("model", model);

        // render container
        VAO.bind();
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

        // swap buffers
        window.display();
    }

    return 0;
}