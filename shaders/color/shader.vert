#version 330 core
layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 color;

out vec3 ourColor;

void main()
{
    gl_Position.xyz = pos;
    gl_Position.w = 1.0;

    ourColor = color;
}
